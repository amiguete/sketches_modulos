//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
configura o modulo en modo STA ou en modo AP
xestion de como amosar a paxina web de configuración
*/

//funcions
bool setup_wifi_sta() {
  /*
  configura o módulo wifi en modo cliente
  sen parámetros de entrada
  sen return
  */
  WiFi.mode(WIFI_STA);//establece o módulo wifi como estación
  delay(10);
  //ler na eeprom o ssid e o password wifi
  dprintln("Reading EEPROM ssid");
  String ssid = readEEPROM(ssidAddress[0],ssidAddress[1]);
  dprintln("Reading EEPROM pass");
  String password = readEEPROM(passAddress[0],passAddress[1]); 
  if ( ssid.length() > 1 && password.length() > 1) {//se se obtivo un ssid da EEPROM
    WiFi.begin(ssid, password);//pasar parámetros
    dprint("Connecting to wifi.");
    while (WiFi.status() != WL_CONNECTED) {//realizar conexión
      //indica con parpadeo rápido que está conectandose
      blinking(1,200);//parpadea unha vez cunha duración de 100 ms
      dprint(".");
    }
    dprintln("");
    dprintln("WiFi conected");
    dprintln("IP address: ");
    dprintln(WiFi.localIP());
    return true;
  } else{//se non se obtivo ssid pasar a modo configuración
    dprintln("No valid ssid");
    dprintln("activating AP");
    return false;
  }
}

void setup_AP(){
  /*
  configura o módulo wifi en modo punto de acceso
  sen parámetros de entrada
  sen return
  */
  WiFi.mode(WIFI_AP);//establece o módulo wifi como punto de acceso
  delay(10);
  dprintln("activating Access Point");
  dprint("SSID:");
  dprintln(ssidAp);
  dprint("password:");
  dprintln(passwordAp);
  //WiFi.softAP(nome da rede, passwordAp, canal, escondida, numero maximo de conexións) 
  WiFi.softAP(ssidAp, passwordAp, 6, false, 1);
  dprint("IP address:");
  dprintln(WiFi.softAPIP());
}

void createWeb(){
  /*
  define o contido e o comportamento da paxina que se amosa
  é unha paxina onde se poden definir os parametros de configuración
  para comunicar co servidor
  sen parámetros de entrada
  sen return
  */  
  server.begin();// iniciar servidor
  server.on("/", []() {//define que se amosa cando se accede á raiz da web
    if (SPIFFS.exists("/index.html")){
      dprint("found index.html on SPIFFS");
      File indexFile = SPIFFS.open("/index.html", "r");
      server.streamFile(indexFile, "text/html"); 
      indexFile.close(); 
    } else{
      server.send(500, "text/plain", "index.html not found in server");
    }
    
  });
  server.on("/set", HTTP_POST, []() {
    //tomar valores pasados como parametros dende o formulario amosado en /
    String new_ssid = server.arg("ssid");
    String new_password = server.arg("password");
    String new_mqttuser = server.arg("mqttuser");
    String new_mqttpass = server.arg("mqttpass");    
    String new_startStatus = server.arg("startStatus"); 
    String new_alias = server.arg("alias");
    String new_cacert = server.arg("cacert");
    String new_client_cert = server.arg("client_cert");
    String new_client_key = server.arg("client_key");
    String new_mqttaddress = server.arg("mqttaddress"); 
    String new_mqttport = server.arg("mqttport"); 
    String content;//contido da web amosada en modo AP
    int statusCode;//codigo de estado http
    //verificase se hai cambios, cal/cales, e cambiase o seu valor na EEPROM
    dprint("writing eeprom ssid: ");
    dprintln(new_ssid);
    writeEEPROM(ssidAddress[0],ssidAddress[1], new_ssid);
    dprint("writing eeprom password: ");
    dprintln(new_password);
    writeEEPROM(passAddress[0],passAddress[1], new_password);
    dprint("writing eeprom mqttuser: ");
    dprintln(new_mqttuser);
    writeEEPROM(mqttUserAddress[0],mqttUserAddress[1], new_mqttuser);
    dprint("writing eeprom password: ");
    dprintln(new_mqttpass);
    writeEEPROM(mqttPassAddress[0],mqttPassAddress[1], new_mqttpass);
    dprint("writing eeprom startStatus: ");
    dprintln(new_startStatus);
    writeEEPROM(startStatusAddress[0],startStatusAddress[1], new_startStatus);
    dprint("writing eeprom alias: ");
    dprintln(new_alias);
    writeEEPROM(aliasAddress[0],aliasAddress[1], new_alias);
    dprint("writing eeprom cacert: ");
    writeEEPROM(cacertAddress[0],cacertAddress[1], new_cacert);
    dprintln("");
    dprint("writing eeprom client_cert: ");
    writeEEPROM(client_certAddress[0],client_certAddress[1], new_client_cert);
    dprintln("");
    dprint("writing eeprom client_key: ");
    writeEEPROM(client_keyAddress[0],client_keyAddress[1], new_client_key);
    dprintln("");
    dprint("writing eeprom mqtt address: ");
    writeEEPROM(mqtt_serverAddress[0],mqtt_serverAddress[1], new_mqttaddress);
    dprintln("");    
    dprint("writing eeprom mqtt port: ");
    writeEEPROM(mqtt_portAddress[0],mqtt_portAddress[1], new_mqttport);
    dprintln(""); 
    content = "<!DOCTYPE HTML>\n<html>";
    content += "\n<head>\n<title>configuración gardada</title>\n";
    content += "<meta charset=\"utf-8\"\n>";
    content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n</head>";
    content += "\n<body><p>Configuración gardada no dispositivo.</p>";
    content += "\n<p>O dispositivo reiniciarase en 3 segundos.</p>";
    content += "</body>\n</html>";
    statusCode = 200;
    server.send(statusCode, "text/html", content);
    blinking(6,500);
    ESP.restart();
  });
  server.onNotFound([]() {
    String content;
    int statusCode = 404;//codigo de estado http
    content = "<!DOCTYPE HTML>\n<html>";
    content += "\n<head>\n<title>Error 404 - a páxina non existe</title>\n";
    content += "<meta charset=\"utf-8\"\n>";
    content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n</head>";
    content += "\n<body><p>esta web de configuración só ten contido en ";
    content += " <a href=\"http://192.168.4.1\">http://192.168.4.1</a></p></body>";
    content += "\n</html>";
    server.send(statusCode, "text/html", content);
  });
  dprintln("Server started");
}
