//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
pines elexidos para IO
*/
#if DEVICE == MINI
  const int rele = D3;//número de saida dixital onde se conecta o relé
  const int led = LED_BUILTIN;//conexión a led
  const int boton = D2;//conexión a botón
#endif
#if DEVICE == SONOFF
  const int rele = 12;//número de saida dixital onde se conecta o relé
  const int led = 13;//conexión a led
  const int boton = 0;//conexión a botón
#endif
#if DEVICE == ESP01
  const int rele = 2;//número de saida dixital onde se conecta o relé
  const int led = 1;//conexión a led
  const int boton = 0;//conexión a botón
#endif
