//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
pines elexidos para IO
*/
#if DEVICE == MINI
  const int led = LED_BUILTIN;//conexión a led
  const int botonR = D2;//conexión a botón Vermello
  const int botonG = D3;//conexión a botón Verde
  const int botonB = D6;//conexión a botón Azul
#endif
#if DEVICE == ESP01
  const int led = 1;//conexión a led
  const int botonR = 0;//conexión a botón Vermello
  const int botonG = 2;//conexión a botón Verde
  const int botonB = 3;//conexión a botón Azul
#endif
