//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
  cliente MQTT para ESP8266 que lee pulsacións de botóns e 
  publica unha mensaxe cando son pulsados
  - conexión wifi
    modo station, ip dinámica
    parpadeo ledbuiltin durante conexión
    os parametros están na memoria EEPROM,nos enderezos:
       ssid: 0-31
       password: 32-95
    se non hai parametros na EEPROM ou se pulsa o botón de configuración
    durante o inicio crea un punto de acceso e inicia un servidor
    web en 192.168.4.1 para poder configurar os parametros da wifi, da conexión
    mqtt, un alias para o dispositivo, o estado no que debe estar o relé
    ó inicio do programa, e a carga dos certificados precisos. o alias, 
    e os certificados están nos enderezos:
      alias: 196-198
      certificado CA: 256-1510
      certificado de cliente: 1511-2460
      chave privada de cliente: 2461-3403
  - conexión a broker
    usuario e contrasinal definidos na EEPROM e cambiables por webconfig
    os parametros están na memoria EEPROM,nos enderezos:
       mqttuser: 96-127
       mqttpass: 128-191    
    tamén se pode configurar o enderezo ip e o porto do broker para poder
    conectarse na wifi da casa ou na do emitida polo servidor caseiro
    os parametros están na memoria EEPROM,nos enderezos:
       mqtt_serverAddress: 3404-3420
       mqtt_portAddress: 3421-3425
    conexión establecida con autenticación ssl do servidor mediante CA
    publica ip, id, e alias mediante o topic "connection/outTopic"
    subscripción a petición status
    reconexión automática no loop.

  - lectura de 3 pines conectados a pulsadores
    emite unha mensaxe mqtt cando un botón é pulsado

  - control do estado do dispositivo a recibir co topic "status"
    publica IdCliente + nivel de batería co topic "status/outTopic"
    
  - opción baixo consumo configurable por webconfig, se é elexida
    o módulo pasará a estado inactivo ata que se pulse o botón de 
    reset cada vez que pase o tempo definido tinactivo
*/

//librerias
#include <ESP8266WiFi.h>//conexión wifi
#include <ESP8266WebServer.h>//facer de servidorweb
#include <PubSubClient.h>//cliente MQTT
#include <EEPROM.h>//libreria para datos non volatiles
#include <FS.h>   // mamexar ficheiros en SPIFFS

//constantes
#include "configuracions.h"//do ap, do servidor, dos topics, e o a autoridade certificadora
const int ssidAddress[] = {0,31};//enderezo inicio e fin na EEPROM do SSID
const int passAddress[] = {32,95};//enderezo inicio e fin na EEPROM do password wifi
const int mqttUserAddress[] = {96,127};//enderezo inicio e fin na EEPROM do usuario mqtt
const int mqttPassAddress[] = {128,195};//enderezo inicio e fin na EEPROM do password mqtt
const int powerSavingAddress[] = {196,198};//enderezo inicio e fin na EEPROM do modo aforro enerxía
const int aliasAddress[] = {199,255};//enderezo inicio e fin na EEPROM do password mqtt
const int cacertAddress[] = {256,1510};//enderezo inicio e fin na EEPROM do certificado CA
const int client_certAddress[] = {1511,2460};//enderezo inicio e fin na EEPROM do certificado cliente
const int client_keyAddress[] = {2461,3403};//enderezo inicio e fin na EEPROM da chave do cliente
const int mqtt_serverAddress[] = {3404,3420};//enderezo inicio e fin na EEPROM da ip do broker
const int mqtt_portAddress[] = {3421,3425};//enderezo inicio e fin na EEPROM do porto do broker
const char empty_cert[] = "";//preciso para iniciar un X509list global
ADC_MODE(ADC_VCC);//ignora o pin A0 e mide voltaxe de entrada

//variables:
#include "pineado.h"//pines elexidos para IO
ESP8266WebServer server(80);//servidor web atende o porto 80
WiFiClientSecure wificlient;//conexión wifi con ssl
PubSubClient mqttclient(wificlient);//conexión co broker
X509List trustedCA(empty_cert);//lista de certificados de CA válidas
X509List trustedClient(empty_cert);//lista de certificados de clientes
PrivateKey privateKey;//chave privada do cliente
String clientId;//identificador único de cliente
String alias;//nome do dispositivo facilitado polo usuario
char mqtt_server[16];//enderezo IP do broker MQTT
unsigned int mqtt_port;//porto de escoita do broker MQTT
bool modeConfig;//cando é true o modulo inicia en modo de configuración
char cert[1300];//variable para almacenar certificados co tamaño do maior
bool powerSaving;//e true cando está activado o aforro de enerxía
unsigned long t0;//usada para controlar o tempo entre pulsacións

//executar 1 vez ó inicio
void setup() {
  //inicializar pines de saida
  pinMode(led, OUTPUT);
  pinMode(botonR, INPUT);  
  pinMode(botonG, INPUT);
  pinMode(botonB, INPUT);
  #if DEBUG == 1 //se está activado o debug inicia a conexión serie
    Serial.begin(9600);
    #define dprintln(x)  Serial.println(x)
    #define dprint(x)  Serial.print(x)
  #else
    #define dprintln(x)
    #define dprint(x)
  #endif
  EEPROM.begin(4096);//iniciar EEPROM con reserva de 4096 b
  if (!SPIFFS.begin()) {
    dprintln("Failed to mount file system");
    return;
  }  delay(3000);//agarda tres segundos para poder pulsar o botón
  //no ESP01 non se pode iniciar con el pulsado porque entra en modo flash
  //se o botón está pulsado despois dese segundo, entra en modo configuración
  if(digitalRead(botonR)==LOW){//boton con pullup para que sirva tamén para modo flash
    modeConfig = true;
    dprintln("config mode activated");
  } else{
    modeConfig = false;
    dprintln("run in normal Mode");
  }
  delay(10);
  //dar identificador único ó módulo 
  clientId = "botonera-";
  clientId += WiFi.macAddress();
  //ler da EEPROM o alias do modulo
  dprintln("Reading EEPROM alias");
  alias = readEEPROM(aliasAddress[0],aliasAddress[1]);
  if (alias.length() < 1){//se non hai alias definido
    alias = clientId;//usa o identificador de cliente
  }
  //leer da EEPROM o modo de aforro enerxía
  dprintln("Reading EEPROM powerSaving");
  String strPowerSaving = readEEPROM(powerSavingAddress[0],powerSavingAddress[1]);
  powerSaving = strPowerSaving == "ON";
  if (modeConfig){//se está en modo configuración
    setup_AP();//iniciar Punto de acceso para servir web de configuración   
    createWeb();//definir contido e comportamento da web
  }
  else{//está en modo de traballo normal
    //configurar wifi
    if (setup_wifi_sta()){//se conectou con exito
      //client.setInsecure(); //non verifica o servidor, só en caso de probas      
      dprintln("Reading mqtt server");
      readEEPROM(mqtt_serverAddress[0],mqtt_serverAddress[1]).toCharArray(mqtt_server,16);
      dprintln("Reading mqtt port");
      mqtt_port = readEEPROM(mqtt_portAddress[0],mqtt_portAddress[1]).toInt();    
      setClock();//tomar a hora do servidor NTP_server
      //obter o certificado CA da memoria EEPROm
      dprintln("read cacert");
      fromEEPROMToCert(cacertAddress[0],cacertAddress[1]);
      trustedCA.append(cert);//engadir o certificado a lista trustedCA      
      wificlient.setTrustAnchors(&trustedCA);//utilizar certificados da lista para verificar server
      //ler o certificado do cliente da EEPROM
      dprintln("read client_cert");
      fromEEPROMToCert(client_certAddress[0],client_certAddress[1]);
      trustedClient.append(cert);//engadir o certificado lido á lista trutedClient
      //ler a chave privada de cliente da EEPROm
      dprintln("read client_key");
      fromEEPROMToCert(client_keyAddress[0],client_keyAddress[1]);
      privateKey.parse(cert);
      wificlient.setClientRSACert(&trustedClient, &privateKey);//usar os certificados de cliente
      mqttclient.setServer(mqtt_server, mqtt_port);//definir broker
      mqttclient.setCallback(callback);//definir función de recepción
      t0 = millis();//inicializa o contador
    } else {//se non consigue parametros de wifi
      modeConfig = true;//para que no loop xestione o servidor web
      setup_AP();
    }
  }
}

//executar en bucle indefinidamente
void loop() {
  delay(100);
  //xestión de servidor ou de conexión mqtt
  if (modeConfig){//se está en modo configuración
      server.handleClient();
      blinking(1,1000);
  } else{//está en modo de traballo normal
    if (!mqttclient.connected()) {//se o cliente non está conectado
      mqtt_conexion();//chama a función que o conecta
    }
    mqttclient.loop();//quedar á espera de mensaxes
    bool restartTime = read_button();
    if (powerSaving){
      digitalWrite(led, LOW);//encender led para indicar que está desperto
      unsigned long tnow = millis();//valor do tempo agora
      if (restartTime){//cada vez que se pulsa o botón
        t0 = tnow;//reiniciase o contador
      }
      dprint("tnow - t0 = ");
      dprintln(tnow-t0);
      if ((tnow - t0) > tinactive){//se pasou tinactive sen uso
        dprintln("entrando en modo baixo consumo");
        ESP.deepSleep(0);//baixo consumo ata pulsar o reset
      }
  }
  }
}
