//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
funcións que non teñen cabida noutras categorías
*/

//funcións 
void setClock() {
  /*
  obten a hora do servidor local, é preciso para a verficación de validez de certificados
  sen parametros de entrada
  sen return
  */
  configTime(1 * 3600, 0, mqtt_server, NTP_server1, NTP_server2);//gmt + 1, horario de verán, ip servidor
  dprint("Waiting for NTP time sync: ");
  time_t now = time(nullptr);//obter a hora do sistema para ver se é dende o reinicio ou a universal
  byte retries = 0;//para contar o número de reintentos de obter a hora dun servidor
  while (now < 8 * 3600 * 2) {//now devolve un número moi baixo mentres non obtén a hora universal
    retries ++;
    delay(500);
    dprint(".");
    now = time(nullptr);//novo intento de obter a hora dun dos servidores
    if (retries > 20)//cando pasa un número de reintentos de conectar cos servidores...
      now = 1679769000;//...establece a hora manualmente a 25/03/2023 18:30
  }
  dprintln("");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  dprint("Current time: ");
  dprint(asctime(&timeinfo));
}

void read_button (){
  /*
  lee o botón definido na variable global,
  publica o valor da lectura do sensor
  está función sempre tarda 80 ms en executarse
  contrario
  */
  //lectura de pulsador
  if(digitalRead(boton)==LOW){//se está pulsado o botón
    publish_status("value", lastT, lastH); //publica o valor dos sensores
  }
  delay(100);
}

void blinking(int times, int duration){
    /*
  fai parpadear o led definido na variable global led o número de 
  veces definido no parametro times, e o tempo total de encendido+
  apagado é duration de cada vez. durante o parpadeo o botón tamén
  se comproba
  times ten que ser > 0 e duration > de 100
  */
  for (int i = 0; i<=times; i++){
    digitalWrite(led, LOW);
    for (int j = 0; j<duration/2;j+=50){
      delay(50);
    }
    digitalWrite(led, HIGH);
    for (int j = 0; j<duration/2;j+=50){
      delay(50);
    }
  }
}
