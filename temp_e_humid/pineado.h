//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
pines elexidos para IO
*/
#if DEVICE == MINI
  const int led = LED_BUILTIN;//conexión a led
  const int boton = D2;//conexión a botón
  const int sensor = D3;//conexión a DHT11
#endif
#if DEVICE == ESP01
  const int led = 1;//conexión a led
  const int boton = 0;//conexión a botón
  const int sensor = 2;//conexión a DHT11
#endif
