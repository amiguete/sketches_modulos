//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
constantentes cos parametros de configuración do punto de acceso wifi
da conexión co servidor mqtt, do servidor de tempo, e dos topics
nos que publica e ós que estará subscrito.
se cambian os datos no servidor basta con cambialos aquí.
*/
#define DEBUG 1//se é 1 compila os dprint se non ignoraos
#define MINI 1
#define ESP01 2
#define DEVICE MINI//MINI para wemos D1 mini
const char* ssidAp = "ESPconfig";//nome da rede en modo AP
const char* passwordAp = "testpass";//password da rede en modo AP
const char* NTP_server1 = "150.214.94.5";//enderezo do servidor NTP de hora.roa.es
const char* NTP_server2 = "158.227.98.15";//enderezo do servidor NTP de ntp.i2t.ehu.es
const String topicStatus = "status";//topic de petición de status
const String topicOut = "deviceOut";//topic de envío de mensaxes
const int frecuencyLecture = 2000;//cada canto tomar unha lectura en ms(min 1000 recomendado 5000)
