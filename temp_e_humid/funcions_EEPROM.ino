//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
dados os enderezos do dato podese leer ou escribir strings neses
enderezos
*/

//funcións 
String readEEPROM(int ini,int fin){
  /*
  dados un valor de  inicio e un valor de fin devolve un string 
  formado polos caracteres que se atopan na EEPROM nas posicións
  de memoria entre inicio e fin ambas incluidas.
  recibe o enderezo de inicio e o de fin    
  devolve o string leido
  */
  String value;
  dprint("\tRead: ");
  for (int i = ini; i <= fin; ++i){//lee letra a letra
    value += char(EEPROM.read(i));
    yield();//oportunidade de execución da pila TCP/IP
  }
  dprintln(value);
  return value;
}

void fromEEPROMToCert(int ini, int fin){
  /*
  dados un valor de  inicio e un valor de fin escribe en cert 
  (variable global) o valor obtido na EEPROM neses enderezos
  recibe o enderezo de inicio e o de fin 
  sen parametro de retorno
  */
  for (int i = ini; i <= fin; ++i){
    cert[i-ini]= EEPROM.read(i);//lee letra a letra
    dprint(cert[i-ini]);
    yield();//oportunidade de execución da pila TCP/IP
  }
  dprintln("");
}

void writeEEPROM(int ini, int fin, String value){
  /*
  dados un valor de  inicio e un valor de fin escribe un string 
  na EEPROM nas posicións de memoria entre inicio e fin ambas
  incluidas. No espacio sobrante escríbense 0.
  se o string é unha cadea baleira conserva o dato anterior
  recibe o enderezo de inicio e o de fin, e o string a gardar  
  sen parametro de retorno
  */
  if (value.length() > 0){//se o string está baleiro non fai nada
    //pasa o novo valor a EEPROM
    dprintln("\tWrite: ");
    for (int i = 0; i < value.length(); ++i){
      EEPROM.write(i+ini, value[i]);      
      dprint(value[i]); 
      yield();
    }
    dprintln("");
    //e "borra" o resto do espacio reservado
    for (int i = ini + value.length(); i <= fin; ++i){
      EEPROM.write(i, 0);       
      yield();
    }    
    EEPROM.commit();//salvado de datos na EEPROM
  }
}
