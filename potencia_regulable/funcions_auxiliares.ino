//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
funcións que non teñen cabida noutras categorías
*/

//funcións 
void setClock() {
  /*
  obten a hora do servidor local, é preciso para a verficación de validez de certificados
  sen parametros de entrada
  sen return
  */
  configTime(1 * 3600, 0, mqtt_server, NTP_server1, NTP_server2);//gmt + 1, horario de verán, ip servidor
  dprint("Waiting for NTP time sync: ");
  time_t now = time(nullptr);//obter a hora do sistema para ver se é dende o reinicio ou a universal
  byte retries = 0;//para contar o número de reintentos de obter a hora dun servidor
  while (now < 8 * 3600 * 2) {//now devolve un número moi baixo mentres non obtén a hora universal
    retries ++;
    delay(500);
    dprint(".");
    now = time(nullptr);//novo intento de obter a hora dun dos servidores
    if (retries > 20)//cando pasa un número de reintentos de conectar cos servidores...
      now = 1679769000;//...establece a hora manualmente a 25/03/2023 18:30
  }
  dprintln("");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  dprint("Current time: ");
  dprint(asctime(&timeinfo));
}

void read_button (){
  /*
  lee os botóns definido na variables globais, e aumenta ou diminue
  en función de que botón está pulsado
  está función sempre tarda 50 ms en executarse
  */
  //lectura de pulsador
    if(digitalRead(botonMas)==LOW){//se está pulsado o botón de aumento
      write_power(powerValue + 5);//aumenta de 5 en 5(20 niveis)

    } else if(digitalRead(botonMenos)==LOW) {//se está pulsado o botón de diminuir
      write_power(powerValue - 5);//diminue
    }
    delay(50);
}

void write_power(int power){
    /*
  mantén os valores da potencia entre 0 e 100
  mapea o valor a 1024 para a saida PWM
  publica o valor
  */
  if (power > 100){//non permite que supere o valor máximo
  power = 100;
  }
  if (power < 0){//evita valores negativos
    power = 0;
  }
  powerValue = power;  
  power = map(powerValue, 0, 100, 0, 1024);
  dprint("power value: ");
  dprintln(power);
  analogWrite(potencia, power);//cambia a potencia de saída
  if (!modeConfig){//se non está en modo configuración
    publish_status("value"); //publica o valor da potencia
  }
}

void blinking(int times, int duration){
    /*
  fai parpadear o led definido na variable global led o número de 
  veces definido no parametro times, e o tempo total de encendido+
  apagado é duration de cada vez. durante o parpadeo o botón tamén
  se comproba
  times ten que ser > 0 e duration > de 100
  */
  for (int i = 0; i<=times; i++){
    digitalWrite(led, LOW);
    for (int j = 0; j<duration/2;j+=50){
      read_button();//inclue un delay de 50 ms
    }
    digitalWrite(led, HIGH);
    for (int j = 0; j<duration/2;j+=50){
      read_button();
    }
  }

}
