//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
pines elexidos para IO
*/
#if DEVICE == MINI
  const int potencia = D3;//número de saida dixital onde se conecta o mosfet
  const int led = LED_BUILTIN;//conexión a led
  const int botonMas = D2;//conexión a botón de aumento potencia
  const int botonMenos = D1;//conexión a botón de disminución potencia
#endif
#if DEVICE == ESP01
  const int potencia = 2;//número de saida dixital onde se conecta o mosfet
  const int led = 1;//conexión a led
  const int botonMas = 0;//conexión a botón de aumento potencia
  const int botonMenos = 3;//conexión a botón de disminución potencia
#endif
