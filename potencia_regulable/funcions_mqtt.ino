//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
xestión da conexión á servidor mqtt cas correspondentes subscripcións
e xestión de que facer cando se recibe unha mensaxe cun dos topics
ós que está subscrito
*/

//funcións 

void mqtt_conexion() {
  /*
  fución de conexión e reconexión a brocker MQTT
  sen parámetros de entrada
  sen return
  codigos de error mqttclient.state:
  
    -4 : MQTT_CONNECTION_TIMEOUT - the server didn't respond within the keepalive time
    -3 : MQTT_CONNECTION_LOST - the network connection was broken
    -2 : MQTT_CONNECT_FAILED - the network connection failed
    -1 : MQTT_DISCONNECTED - the client is disconnected cleanly
    0 : MQTT_CONNECTED - the client is connected
    1 : MQTT_CONNECT_BAD_PROTOCOL - the server doesn't support the requested version of MQTT
    2 : MQTT_CONNECT_BAD_CLIENT_ID - the server rejected the client identifier
    3 : MQTT_CONNECT_UNAVAILABLE - the server was unable to accept the connection
    4 : MQTT_CONNECT_BAD_CREDENTIALS - the username/password were rejected
    5 : MQTT_CONNECT_UNAUTHORIZED - the client was not authorized to connect

  */
  //ler na eeprom o usuario e o password mqtt
  dprintln("Reading EEPROM mqtt_username");
  String mqtt_username = readEEPROM(mqttUserAddress[0],mqttUserAddress[1]);
  dprintln("Reading EEPROM mqtt_password");
  String mqtt_password = readEEPROM(mqttPassAddress[0],mqttPassAddress[1]);
  while (!mqttclient.connected()) {//ata que non consiga conexión
    dprintln("Atempt MQTT connection...");
    blinking(3,100);
    dprint(".");
    read_button();
    if (mqttclient.connect(clientId.c_str(),mqtt_username.c_str(), mqtt_password.c_str())) {//se conecta
      dprintln("");
      dprint(clientId);
      dprintln(" connected");
      // cando consegue conexión publica unha mensaxe...
      String message = clientId;
      message +="|/|";
      message += WiFi.localIP().toString();
      message +="|/|";
      message += alias;
      String topic = topicOut +"/connection";//topic adicado a novas conexións
      mqttclient.publish(topic.c_str(), message.c_str());
      // ... e [re]subscribese os topics
      topic = clientId + "/" + topicPower;
      mqttclient.subscribe(topic.c_str(),1);
      topic = topicStatus;//status dirixirase a tódolos módulos
      mqttclient.subscribe(topic.c_str());
      blinking(3,100);
    } else {//se da fallo
      dprint("fail, rc=");
      dprint(mqttclient.state());
      //agarda 3 segundos e tenta de novo
      dprintln(" retry in 3 seconds");
      delay(3000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  /*
  función que se chama cando se recibe unha mensaxe nun dos topics
  ós que se está subscrito:
    se o topic é action:
      cambia a potencia de saida e publica o valor
    se o topic é status publica mensaxe co topic outTopic e co valor
    da potencia actual
  recibe o topic, a mensaxe, e a lonxitude da mensaxe
  sen return
  */
  String strTopic((char*)topic);//converte o *char a String para poder verificalo
  dprint("incomming message [");
  dprint(strTopic);
  dprint("] ");
  String strPayload;//converte o *char a String para poder verificalo
  for (int i = 0; i < length; i++) {
    dprint((char)payload[i]);
    strPayload += (char)payload[i];
  }
  dprintln();
  if(strTopic==clientId + "/" + topicPower){//se o topic era o de potencia
    dprintln("topic = " + topicPower);
      write_power(strPayload.toInt());
  }
  if(strTopic==topicStatus){//se o topic era o de status
    dprintln("topic = " + topicStatus);
    publish_status("status");
  }
  blinking(2,100);//indicar con un parpadeo que se recibiu mensaxe

}

void publish_status(String valueOrStatus){
    /*
  función que se chama cada vez que se recibe a mensaxe status
  ou cambia o estado por pulsarse o botón, ou por
  recibir unha mesaxe en /action
  comproba o estado do pin e responde en función do mesmo co topic
  value ou o de status en función do que se recibira como valueOrStatus
  sen return
  */
  String msg = clientId;
  msg += "|/|";
  msg += powerValue;
  if (valueOrStatus == "status"){
    //o alias só se publica en status
    msg += "|/|";
    msg += alias;
  }
  String topic = topicOut;
  topic += "/";  
  topic += valueOrStatus;//construe o topic co recibido en valueOrStatus
  mqttclient.publish(topic.c_str(), msg.c_str());//publica o estado 
}
