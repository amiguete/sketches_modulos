//creado por: Adrián Concheiro Garea
//            a.concheiro@udc.es
//Descripción:
/*
pines elexidos para IO
*/
#if DEVICE == MINI
  const int led = LED_BUILTIN;//conexión a led
  const int boton = D2;//conexión a botón
  const int sensor = A0;//conexión a foto resistencia
#endif
