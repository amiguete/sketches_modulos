proxecto que inclúe todo o preciso para montar os módulo incluídos no proxecto de Enrelar. Contén os sketches que se poden cargar co IDE de Arduino, e os esquemas para poder montalos. Os módulos creados son:
- Un relé que permite acender e apagar aparatos eléctricos.
- Un control de potencia que permite regular con PWM aparatos de corrente continua como tiras de LED ou ventiladores.
- Unha botonera que envía unha mensaxe ó sitema cada vez que un botón é premido.
- Un sensor de temperatura e humidade que se comporta como 2 módulos independentes.
- Un sensor de luminosidade.